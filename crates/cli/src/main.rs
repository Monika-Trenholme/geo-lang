use std::{
    env,
    fs::{self, File},
    path::Path,
    process::exit,
};

use lang::render;

fn main() {
    let mut args = env::args();
    args.next();
    let mut input: Option<String> = None;
    for arg in args.by_ref() {
        match arg.as_str() {
            "--" => break,
            _ => {
                if input.is_none() {
                    input = Some(arg);
                    continue;
                }
            }
        }
        eprintln!("Extraneous argument: {arg}");
        exit(1);
    }

    match input {
        Some(file) => {
            let source = match fs::read_to_string(&file) {
                Ok(source) => source,
                Err(e) => {
                    eprintln!("Error opening file `{file}`: {e}");
                    exit(1);
                }
            };
            let mut out = match File::create(Path::new(&file).with_extension("svg")) {
                Ok(file) => file,
                Err(e) => {
                    eprintln!("Error creating output file: {e}");
                    exit(1);
                }
            };
            match render(source, &mut out, args.collect()) {
                Ok(_) => (),
                Err(e) => {
                    eprintln!("{e}");
                    exit(1);
                }
            };
        }
        None => {
            eprintln!("No input file given");
            exit(1);
        }
    }
}
