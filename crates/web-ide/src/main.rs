use js_sys::Array;
use lang::render;
use wasm_bindgen::prelude::*;
use web_sys::{
    Blob, Document, DomParser, HtmlAnchorElement, KeyboardEvent, KeyboardEventInit, Node,
    SupportedType, Url,
};

#[wasm_bindgen(raw_module = "./editor.js")]
extern "C" {
    fn get_code() -> String;
}

fn main() {
    let window = web_sys::window().unwrap();
    let document = window.document().unwrap();
    let body = document.body().unwrap();

    let dom_parser = DomParser::new().unwrap();

    let mut image = String::new();

    let mut on_keydown = move |event: KeyboardEvent| {
        if !event.ctrl_key() {
            return;
        }
        match event.key().as_ref() {
            "r" => render_code(&mut image, &dom_parser, &body, &document),
            "s" => save_string(&image, &document),
            _ => (),
        }
    };

    // Render default program
    let mut keyboard_event = KeyboardEventInit::new();
    keyboard_event.ctrl_key(true).key("r");
    on_keydown(KeyboardEvent::new_with_keyboard_event_init_dict("down", &keyboard_event).unwrap());

    let on_keydown = Closure::new(on_keydown);
    window.set_onkeydown(Some(on_keydown.as_ref().unchecked_ref()));
    on_keydown.forget();
}

fn render_code(image: &mut String, dom_parser: &DomParser, body: &Node, document: &Document) {
    image.clear();
    match render(get_code(), image, Vec::new()) {
        Ok(_) => {
            let svg = dom_parser
                .parse_from_string(&image, SupportedType::ImageSvgXml)
                .unwrap()
                .document_element()
                .unwrap();
            svg.set_id("output");
            body.replace_child(&svg, document.get_element_by_id("output").unwrap().as_ref())
                .unwrap();
        }
        Err(e) => {
            let message = format!("{e}");
            web_sys::window()
                .unwrap()
                .alert_with_message(&message)
                .unwrap();
        }
    }
}

fn save_string(data: &String, document: &Document) {
    let array = Array::new();
    array.push(&JsValue::from(data));
    let blob = Blob::new_with_str_sequence(&array).unwrap();

    let url = Url::create_object_url_with_blob(&blob).unwrap();

    let anchor = document
        .create_element("a")
        .unwrap()
        .dyn_into::<HtmlAnchorElement>()
        .unwrap();
    anchor.set_href(&url);
    anchor.set_download("construction.svg");
    anchor.click();

    Url::revoke_object_url(&url).unwrap();
}
