use std::{
    cmp::{Eq, Ord, Ordering, PartialEq, PartialOrd},
    fmt, ops,
};

#[derive(Clone, Copy)]
pub struct Number(pub f64);

impl Number {
    pub const ZERO: Self = Self(0.0);
    pub const ONE: Self = Self(1.0);
    pub const TWO: Self = Self(2.0);
    pub const PI: Self = Self(3.141592653589793);

    pub fn sq(&self) -> Self {
        Self(self.0.powf(2.0))
    }

    pub fn sqrt(&self) -> Self {
        Self(self.0.sqrt())
    }

    pub fn atan2(&self, other: &Self) -> Self {
        Self(self.0.atan2(other.0))
    }
}

impl fmt::Display for Number {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl PartialEq for Number {
    fn eq(&self, other: &Self) -> bool {
        (self.0 - other.0).abs() < 1e-10
    }
}

impl Eq for Number {}

impl PartialOrd for Number {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if self == other {
            Some(Ordering::Equal)
        } else if self.0 > other.0 {
            Some(Ordering::Greater)
        } else {
            Some(Ordering::Less)
        }
    }
}

impl Ord for Number {
    fn cmp(&self, other: &Self) -> Ordering {
        if self == other {
            Ordering::Equal
        } else if self.0 > other.0 {
            Ordering::Greater
        } else {
            Ordering::Less
        }
    }
}

impl ops::Add<Number> for Number {
    type Output = Self;
    fn add(self, other: Self) -> Self {
        Self(self.0 + other.0)
    }
}

impl ops::Sub<Number> for Number {
    type Output = Self;
    fn sub(self, other: Self) -> Self {
        Self(self.0 - other.0)
    }
}

impl ops::Mul<Number> for Number {
    type Output = Self;
    fn mul(self, other: Self) -> Self {
        Self(self.0 * other.0)
    }
}

impl ops::Div<Number> for Number {
    type Output = Self;
    fn div(self, other: Self) -> Self {
        Self(self.0 / other.0)
    }
}

impl ops::Neg for Number {
    type Output = Self;
    fn neg(self) -> Self {
        Self(-self.0)
    }
}
