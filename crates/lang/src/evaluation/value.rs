use super::{
    super::parser::{Ast, Binding, Definition, Signature},
    element::Element,
    render::{RenderError, Scope},
};
use std::{iter::zip, ops::Index, rc::Rc};

#[derive(Clone)]
pub enum Value {
    Element(Element),
    Integer(isize),
    List(List),
    Function(Function),
}

impl Value {
    pub fn r#type(&self) -> &'static str {
        match self {
            Self::Element(_) => "element",
            Self::Integer(_) => "integer",
            Self::List(_) => "list",
            Self::Function(_) => "function",
        }
    }

    pub fn flatten(self) -> Vec<Value> {
        match self {
            Self::List(list) => {
                let mut flat = Vec::with_capacity(list.len());
                for value in list {
                    match value {
                        list @ Self::List(_) => flat.append(&mut list.flatten()),
                        value => flat.push(value),
                    }
                }
                flat
            }
            _ => vec![self],
        }
    }

    fn matches(&self, binding: &Binding) -> bool {
        match (binding, self) {
            (Binding::Name(_, _) | Binding::Underscore(_), _) => true,
            (Binding::List(bindings, _), Value::List(values)) => {
                if bindings.len() != values.len() {
                    return false;
                }
                for (binding, value) in zip(bindings, values.iter()) {
                    if !value.matches(binding) {
                        return false;
                    }
                }
                true
            }
            (Binding::Integer(n, _), Value::Integer(m)) => n == m,
            (Binding::Pop(pops, rest, _), Value::List(values)) => {
                match rest.as_ref() {
                    Binding::Name(_, _) | Binding::Underscore(_) => {
                        if values.len() < pops.len() {
                            return false;
                        }
                    }
                    Binding::List(rest, _) => {
                        if values.len() != pops.len() + rest.len() {
                            return false;
                        }
                    }
                    _ => unreachable!(),
                }
                let mut values = values.iter();
                for (binding, value) in zip(pops, values.by_ref()) {
                    if !value.matches(binding) {
                        return false;
                    }
                }
                if let Binding::List(rest, _) = rest.as_ref() {
                    for (binding, value) in zip(rest, values) {
                        if !value.matches(binding) {
                            return false;
                        }
                    }
                }
                true
            }
            _ => false,
        }
    }

    pub fn bind(self, binding: &Binding, scope: &mut Scope) -> Result<(), RenderError> {
        match binding {
            Binding::Underscore(_) => (),
            Binding::Name(name, loc) => {
                if scope.insert(name.clone(), self).is_some() {
                    return Err(RenderError::Redefinition(name.clone(), *loc));
                }
            }
            Binding::Integer(n, loc) => match self {
                Value::Integer(m) => {
                    if *n != m {
                        return Err(RenderError::IntegerMatch(*n, m, *loc));
                    }
                }
                v => return Err(RenderError::IntegerMatchType(v.r#type(), *loc)),
            },
            Binding::List(bindings, loc) => {
                if let Value::List(values) = self {
                    if bindings.len() != values.len() {
                        return Err(RenderError::ListLength(bindings.len(), values.len(), *loc));
                    }
                    for (binding, value) in zip(bindings, values) {
                        value.bind(binding, scope)?;
                    }
                } else {
                    return Err(RenderError::ListDestructure(*loc));
                }
            }
            Binding::Pop(pops, rest, loc) => {
                if let Value::List(mut values) = self {
                    match rest.as_ref() {
                        Binding::Name(_, _) | Binding::Underscore(_) => {
                            if values.len() < pops.len() {
                                return Err(RenderError::ListLength(
                                    pops.len(),
                                    values.len(),
                                    *loc,
                                ));
                            }
                        }
                        Binding::List(rest, _) => {
                            if values.len() != pops.len() + rest.len() {
                                return Err(RenderError::ListLength(
                                    pops.len() + rest.len(),
                                    values.len(),
                                    *loc,
                                ));
                            }
                        }
                        _ => unreachable!(),
                    }
                    for pop in pops {
                        values.pop().unwrap().bind(pop, scope)?
                    }
                    Value::List(values).bind(rest, scope)?;
                } else {
                    return Err(RenderError::ListDestructure(*loc));
                }
            }
        }
        Ok(())
    }
}

type NodePtr = Option<Rc<Node>>;
struct Node {
    head: Value,
    tail: NodePtr,
}
#[derive(Clone)]
pub struct List {
    data: NodePtr,
    len: usize,
}

impl List {
    pub fn new() -> List {
        List { data: None, len: 0 }
    }

    pub fn from_vec(vec: Vec<Value>) -> List {
        let mut list = List::new();
        for value in vec.into_iter().rev() {
            list.push(value);
        }
        list
    }

    pub fn len(&self) -> usize {
        self.len
    }

    pub fn pop(&mut self) -> Option<Value> {
        let node = self.data.take();
        match node {
            Some(node) => {
                self.data = node.tail.clone();
                self.len -= 1;
                Some(node.head.clone())
            }
            None => None,
        }
    }

    pub fn push(&mut self, value: Value) {
        let sublist = self.data.take();
        let list = Rc::new(Node {
            head: value,
            tail: sublist,
        });
        self.data.replace(list);
        self.len += 1;
    }

    pub fn prepend(&mut self, other: List) {
        for value in other {
            self.push(value);
        }
    }

    pub fn iter(&self) -> Iter {
        Iter(self.data.as_deref())
    }
}

impl Index<usize> for List {
    type Output = Value;
    fn index(&self, index: usize) -> &Value {
        let mut node = self.data.as_ref().unwrap();
        for _ in 0..index {
            node = node.tail.as_ref().unwrap();
        }
        &node.head
    }
}

impl IntoIterator for List {
    type Item = Value;
    type IntoIter = IntoIter;
    fn into_iter(self) -> IntoIter {
        IntoIter(self)
    }
}

pub struct IntoIter(List);

impl Iterator for IntoIter {
    type Item = Value;
    fn next(&mut self) -> Option<Value> {
        self.0.pop()
    }
}

pub struct Iter<'a>(Option<&'a Node>);

impl<'a> Iterator for Iter<'a> {
    type Item = &'a Value;
    fn next(&mut self) -> Option<&'a Value> {
        match self.0 {
            Some(node) => {
                self.0 = node.tail.as_deref();
                Some(&node.head)
            }
            None => None,
        }
    }
}

#[derive(Clone)]
pub struct Function(Rc<Vec<(Signature, Ast, Vec<Definition>)>>);

impl Function {
    pub fn new(signature: Signature, body: Ast, subdefinitions: Vec<Definition>) -> Self {
        Self(Rc::new(vec![(signature, body, subdefinitions)]))
    }

    pub fn add_definition(
        &mut self,
        signature: Signature,
        body: Ast,
        subdefinitions: Vec<Definition>,
    ) {
        Rc::get_mut(&mut self.0)
            .unwrap()
            .push((signature, body, subdefinitions));
    }

    // Unsafe because the rc may be dropped or the internal vec may be relocated
    pub unsafe fn find_signature(
        &self,
        arguments: &[Value],
    ) -> Option<&'static (Signature, Ast, Vec<Definition>)> {
        for part in &*Rc::as_ptr(&self.0) {
            if part.0.len() != arguments.len() {
                continue;
            }
            let mut all = true;
            for (binding, value) in zip(part.0.iter(), arguments.iter()) {
                all = value.matches(binding);
                if !all {
                    break;
                }
            }
            if all {
                return Some(part);
            }
        }
        None
    }
}
