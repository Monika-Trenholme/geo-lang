use super::{
    super::parser::Style,
    element::{Circle, Element, Point},
    number::Number,
    render::RenderError,
};
use std::fmt;

pub trait Write {
    fn write_fmt(&mut self, args: fmt::Arguments<'_>) -> Result<(), RenderError>;
}

macro_rules! impl_write {
    ($t:ty, $i:path) => {
        impl Write for $t {
            fn write_fmt(&mut self, args: fmt::Arguments<'_>) -> Result<(), RenderError> {
                Ok((self as &mut dyn $i).write_fmt(args)?)
            }
        }
    };
}

impl_write!(std::fs::File, std::io::Write);
impl_write!(String, fmt::Write);

pub fn svg(parts: Vec<(Style, Vec<Element>)>, o: &mut dyn Write) -> Result<(), RenderError> {
    write!(o, "<svg")?;
    write!(o, " viewBox=\"0 0 1 1\"")?;
    write!(o, " xmlns=\"http://www.w3.org/2000/svg\"")?;
    write!(o, ">\n")?;
    for (style, elements) in parts {
        if !matches!(elements.get(0), Some(Element::Point(_))) {
            write!(o, "<path {style} d=\"")?;
        }
        let mut last_point: Option<Point> = None;
        for (i, element) in elements.iter().enumerate() {
            match element {
                Element::LineSegment(_, a, b) => {
                    let (al, bl) = last_point
                        .map(|p| (&p == a, &p == b))
                        .unwrap_or((false, false));
                    let (first, last) = if al || bl {
                        if bl {
                            (b, a)
                        } else {
                            (a, b)
                        }
                    } else if i < elements.len() - 1 {
                        match &elements[i + 1] {
                            Element::LineSegment(_, a2, b2) => {
                                if a == a2 || a == b2 {
                                    (b, a)
                                } else {
                                    (a, b)
                                }
                            }
                            Element::Arc(_, a2, b2) => {
                                if a == a2 || a == b2 {
                                    (b, a)
                                } else {
                                    (a, b)
                                }
                            }
                            _ => (a, b),
                        }
                    } else {
                        (a, b)
                    };
                    if !(al || bl) {
                        write!(o, "M {} {} ", first.x, first.y)?;
                    }
                    write!(o, "L {} {} ", last.x, last.y)?;
                    last_point = Some(*last);
                }
                Element::Circle(Circle(c, rsq)) => {
                    let r = rsq.sqrt();
                    let x1 = c.x + r;
                    let x2 = c.x - r;
                    let y = c.y;
                    write!(o, "M {x1} {y} ")?;
                    write!(o, "A {r} {r} 0 1 0 {x2} {y} ")?;
                    write!(o, "A {r} {r} 0 1 0 {x1} {y} z ")?;
                }
                Element::Arc(Circle(c, rsq), a, b) => {
                    let (al, bl) = last_point
                        .map(|p| (&p == a, &p == b))
                        .unwrap_or((false, false));
                    let (sweep_flag, first, last) = if al || bl {
                        if bl {
                            (1, b, a)
                        } else {
                            (0, a, b)
                        }
                    } else if i < elements.len() - 1 {
                        match &elements[i + 1] {
                            Element::LineSegment(_, a2, b2) => {
                                if a == a2 || a == b2 {
                                    (1, b, a)
                                } else {
                                    (0, a, b)
                                }
                            }
                            Element::Arc(_, a2, b2) => {
                                if a == a2 || a == b2 {
                                    (1, b, a)
                                } else {
                                    (0, a, b)
                                }
                            }
                            _ => (0, a, b),
                        }
                    } else {
                        (0, a, b)
                    };
                    if !(al || bl) {
                        write!(o, "M {} {} ", first.x, first.y)?;
                    }
                    let start = first.angle(c);
                    let mut end = last.angle(c);
                    if end < start {
                        end = end + Number::PI * Number::TWO;
                    }
                    let large_flag = if end - start > Number::PI { 1 } else { 0 };
                    write!(
                        o,
                        "A {} {0} 0 {} {} {} {} ",
                        rsq.sqrt(),
                        large_flag,
                        sweep_flag,
                        last.x,
                        last.y
                    )?;
                    last_point = Some(*last);
                }
                Element::Point(p) => {
                    if i > 0 && !matches!(elements[i - 1], Element::Point(_)) {
                        write!(o, "\"/>\n")?;
                    }
                    write!(
                		o,
                		"<circle cx=\"0\" cy=\"0\" r=\"{}\" fill=\"{}\" transform=\"translate({}, {}) scale(0.5, 0.5)\"/>\n",
                		style.width.as_ref().unwrap(),
                		style.stroke.as_ref().unwrap(),
                		p.x,
                		Number::ONE - p.y,
                	)?;
                    if i < elements.len() - 1 && !matches!(elements[i + 1], Element::Point(_)) {
                        write!(o, "<path {style} d=\"")?;
                    }
                }
                _ => unreachable!(),
            }
        }
        write!(o, "\"/>\n")?;
    }
    write!(o, "</svg>")?;
    Ok(())
}
