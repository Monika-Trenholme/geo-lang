use super::number::Number;
use std::{cmp::Ordering, ops};

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct Point {
    pub x: Number,
    pub y: Number,
}

impl Point {
    pub fn new(x: Number, y: Number) -> Self {
        Self { x, y }
    }

    pub fn dist_sq(&self, other: Self) -> Number {
        (self.x - other.x).sq() + (self.y - other.y).sq()
    }

    pub fn angle(&self, center: &Self) -> Number {
        let dx = self.x - center.x;
        let dy = self.y - center.y;
        let mut a = dy.atan2(&dx);
        if a < Number::ZERO {
            a = a + Number::PI * Number::TWO;
        }
        if a == Number::PI * Number::TWO {
            Number::ZERO
        } else {
            a
        }
    }
}

#[derive(Clone, PartialEq, Eq)]
pub struct Line(pub Option<Number>, pub Number);

#[derive(Clone, PartialEq, Eq)]
pub struct Circle(pub Point, pub Number);

#[derive(Clone, PartialEq, Eq)]
pub enum Element {
    Point(Point),
    Line(Line),
    LineSegment(Line, Point, Point),
    Circle(Circle),
    Arc(Circle, Point, Point),
}

impl Element {
    pub fn point(a: Number, b: Number) -> Self {
        Self::Point(Point::new(a, b))
    }

    pub fn line(a: Point, b: Point) -> Self {
        if a.x == b.x {
            Self::Line(Line(None, a.x))
        } else {
            let m = (a.y - b.y) / (a.x - b.x);
            let c = a.y - m * a.x;
            Self::Line(Line(Some(m), c))
        }
    }

    pub fn line_segment(a: Point, b: Point) -> Self {
        if a.x == b.x {
            if a.y > b.y {
                Self::LineSegment(Line(None, a.x), b, a)
            } else {
                Self::LineSegment(Line(None, a.x), a, b)
            }
        } else {
            let m = (a.y - b.y) / (a.x - b.x);
            let c = a.y - m * a.x;
            if a.x > b.x {
                Self::LineSegment(Line(Some(m), c), b, a)
            } else {
                Self::LineSegment(Line(Some(m), c), a, b)
            }
        }
    }

    pub fn circle(c: Point, p: Point) -> Self {
        Self::Circle(Circle(c, c.dist_sq(p)))
    }

    pub fn arc(c: Point, a: Point, b: Point) -> Self {
        Self::Arc(Circle(c, a.dist_sq(c)), a, b)
    }

    pub fn kind(&self) -> &'static str {
        match self {
            Self::Point(_) => "point",
            Self::Line(_) => "line",
            Self::LineSegment(_, _, _) => "line segment",
            Self::Circle(_) => "circle",
            Self::Arc(_, _, _) => "arc",
        }
    }
}

fn point_and_line(p: &Point, line: &Line) -> Vec<Element> {
    let &Line(m, c) = line;
    if let Some(m) = m {
        if m * p.x + c == p.y {
            vec![Element::Point(*p)]
        } else {
            Vec::new()
        }
    } else if p.x == c {
        vec![Element::Point(*p)]
    } else {
        Vec::new()
    }
}

fn point_and_circle(p: &Point, circle: &Circle) -> Vec<Element> {
    let &Circle(c, rsq) = circle;
    if p.dist_sq(c) == rsq {
        vec![Element::Point(*p)]
    } else {
        Vec::new()
    }
}

fn line_and_line(a: &Line, b: &Line) -> Vec<Element> {
    let (&Line(am, ac), &Line(bm, bc)) = (a, b);
    if am == bm {
        Vec::new()
    } else if am.is_none() {
        let y = bm.unwrap() * ac + bc;
        vec![Element::point(ac, y)]
    } else if bm.is_none() {
        let y = am.unwrap() * bc + ac;
        vec![Element::point(bc, y)]
    } else {
        let am = am.unwrap();
        let bm = bm.unwrap();
        let x = (ac - bc) / (bm - am);
        let y = am * x + ac;
        vec![Element::point(x, y)]
    }
}

fn line_and_circle(line: &Line, circle: &Circle) -> Vec<Element> {
    let (&Line(lm, lc), &Circle(center, rsq)) = (line, circle);
    if lm.is_none() {
        let dxsq = (lc - center.x).sq();
        if dxsq > rsq {
            return Vec::new();
        }
        let dysq = rsq - dxsq;
        return match dysq.cmp(&Number::ZERO) {
            Ordering::Less => Vec::new(),
            Ordering::Equal => vec![Element::point(lc, center.y)],
            Ordering::Greater => {
                let dy = dysq.sqrt();
                [dy, -dy]
                    .into_iter()
                    .map(|dy| Element::point(lc, center.y + dy))
                    .collect::<Vec<Element>>()
            }
        };
    }
    let lm = lm.unwrap();
    let a = lm.sq() + Number::ONE;
    let b = Number::TWO * (lm * (lc - center.y) - center.x);
    let c = (lc - center.y).sq() + center.x.sq() - rsq;
    let discriminant = b.sq() - Number::TWO.sq() * a * c;
    match discriminant.cmp(&Number::ZERO) {
        Ordering::Less => Vec::new(),
        Ordering::Equal => {
            let x = -b / (Number::TWO * a);
            let y = lm * x + lc;
            vec![Element::point(x, y)]
        }
        Ordering::Greater => {
            let root = discriminant.sqrt();
            [root, -root]
                .into_iter()
                .map(|r| {
                    let x = (-b + r) / (Number::TWO * a);
                    let y = lm * x + lc;
                    Element::point(x, y)
                })
                .collect()
        }
    }
}

fn line_segment_and_element(line: &Line, a: &Point, b: &Point, other: &Element) -> Vec<Element> {
    let &Line(m, c) = line;
    let slope = m.is_some();
    (&Element::Line(Line(m, c)) & other)
        .into_iter()
        .filter_map(|element| match element {
            Element::Line(Line(m, c)) => Some(Element::LineSegment(Line(m, c), *a, *b)),
            Element::Point(p) => {
                if slope {
                    if a.x < p.x && p.x < b.x {
                        Some(Element::Point(p))
                    } else {
                        None
                    }
                } else if a.y < p.y && p.y < b.y {
                    Some(Element::Point(p))
                } else {
                    None
                }
            }
            Element::LineSegment(l, a2, b2) => {
                if slope {
                    let a = if a.x > a2.x { *a } else { a2 };
                    let b = if b.x < b2.x { *b } else { b2 };
                    if a.x > b.x {
                        None
                    } else {
                        Some(Element::LineSegment(l, a, b))
                    }
                } else {
                    let a = if a.y > a2.y { *a } else { a2 };
                    let b = if b.y < b2.y { *b } else { b2 };
                    if a.y > b.y {
                        None
                    } else {
                        Some(Element::LineSegment(l, a, b))
                    }
                }
            }
            _ => unreachable!(),
        })
        .collect()
}

fn circle_and_circle(a: &Circle, b: &Circle) -> Vec<Element> {
    let (&Circle(ac, arsq), &Circle(bc, brsq)) = (a, b);
    if ac == bc {
        return Vec::new();
    }
    let (m, c) = if ac.y == bc.y {
        let x = (bc.x.sq() - ac.x.sq() + arsq - brsq) / (Number::TWO * (bc.x - ac.x));
        (None, x)
    } else {
        let dy = bc.y - ac.y;
        let m = (ac.x - bc.x) / dy;
        let c = (bc.x.sq() - ac.x.sq() + bc.y.sq() - ac.y.sq() - brsq + arsq) / (Number::TWO * dy);
        (Some(m), c)
    };
    &Element::Circle(Circle(bc, brsq)) & &Element::Line(Line(m, c))
}

fn arc_and_element(circle: &Circle, start: &Point, end: &Point, other: &Element) -> Vec<Element> {
    let Circle(center, _) = circle;
    (&Element::Circle(circle.clone()) & other)
        .into_iter()
        .filter_map(|element| match element {
            Element::Circle(circle) => Some(Element::Arc(circle, *start, *end)),
            Element::Point(point) => {
                let intersection_angle = point.angle(center);
                let start_angle = start.angle(center);
                let end_angle = end.angle(center);
                if end_angle > start_angle {
                    if intersection_angle > start_angle && intersection_angle < end_angle {
                        Some(Element::Point(point))
                    } else {
                        None
                    }
                } else if intersection_angle > start_angle || intersection_angle < end_angle {
                    Some(Element::Point(point))
                } else {
                    None
                }
            }
            Element::Arc(circle, start2, end2) => {
                let start_angle = start.angle(center);
                let start2_angle = start2.angle(center);
                let mut end_angle = end.angle(center);
                if end_angle < start_angle {
                    end_angle = end_angle + Number::PI * Number::TWO;
                }
                let mut end2_angle = end2.angle(center);
                if end2_angle < start2_angle {
                    end2_angle = end2_angle + Number::PI * Number::TWO;
                }
                let a = if start_angle > start2_angle {
                    (*start, start_angle)
                } else {
                    (start2, start2_angle)
                };
                let b = if end_angle < end2_angle {
                    (*end, end_angle)
                } else {
                    (end2, end2_angle)
                };
                if a.1 > b.1 {
                    return None;
                }
                Some(Element::Arc(circle, a.0, b.0))
            }
            _ => unreachable!(),
        })
        .collect()
}

impl ops::BitAnd for &Element {
    type Output = Vec<Element>;
    fn bitand(self, rhs: Self) -> Self::Output {
        match (self, rhs) {
            (a, b) if a == b => vec![a.clone()],
            (Element::Point(_), Element::Point(_)) => Vec::new(),
            (Element::Point(p), Element::Line(l)) | (Element::Line(l), Element::Point(p)) => {
                point_and_line(p, l)
            }
            (Element::Point(p), Element::Circle(c)) | (Element::Circle(c), Element::Point(p)) => {
                point_and_circle(p, c)
            }
            (Element::Line(a), Element::Line(b)) => line_and_line(a, b),
            (Element::Line(l), Element::Circle(c)) | (Element::Circle(c), Element::Line(l)) => {
                line_and_circle(l, c)
            }
            (Element::LineSegment(l, a, b), other) | (other, Element::LineSegment(l, a, b)) => {
                line_segment_and_element(l, a, b, other)
            }
            (Element::Circle(a), Element::Circle(b)) => circle_and_circle(a, b),
            (Element::Arc(circle, start, end), other)
            | (other, Element::Arc(circle, start, end)) => {
                arc_and_element(circle, start, end, other)
            }
        }
    }
}
