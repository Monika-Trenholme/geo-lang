use super::{
    super::{
        lexer::{LexerError, Loc, Op},
        parser::{
            parse, AssignmentStatement, Ast, AstNode, Binding, DrawStatement, FunctionStatement,
            ParserError,
        },
    },
    element::{Element, Line, Point},
    number::Number,
    svg::{svg, Write},
    value::{Function, List, Value},
};
pub use std::{
    collections::HashMap,
    error::Error,
    fmt::{self, Write as _},
    io,
    iter::{zip, Peekable},
    rc::Rc,
};

enum Evaluation<'a> {
    Expression(Vec<(&'a Ast, bool)>),
    ListItem(Vec<(&'a Ast, bool)>),
    Argument(Vec<(&'a Ast, bool)>),
    Definition(&'a Binding, Vec<(&'a Ast, bool)>),
}

pub type Scope = HashMap<String, Value>;

#[derive(Debug)]
pub enum RenderError {
    Parser(ParserError),
    Lexer(LexerError),
    Io(io::Error),
    Fmt(fmt::Error),
    ArgumentParse(String),
    UndefinedSymbol(String, Loc),
    Redefinition(String, Loc),
    ListDestructure(Loc),
    ListLength(usize, usize, Loc),
    IntegerMatch(isize, isize, Loc),
    IntegerMatchType(&'static str, Loc),
    Ruler(&'static str, &'static str, Op, Loc),
    Compass(&'static str, &'static str, Loc),
    CompassElements(&'static str, usize, Loc),
    CompassLength(usize, Loc),
    ArcPoints(Loc),
    Intersection(&'static str, &'static str, Loc),
    NoIntersection(Loc),
    QualificationValue(&'static str, Loc),
    QualificationElements(&'static str, usize, Loc),
    QualificationOrigin(&'static str, Loc),
    QualificationEmpty(Loc),
    Callee(&'static str, Loc),
    CallSignature(String, Loc),
    IndexOOB(Loc),
    IndexTarget(&'static str, Loc),
    Index(&'static str, Loc),
    Prepend(&'static str, Loc),
    Concat(&'static str, &'static str, Loc),
    Numeric(&'static str, &'static str, Loc),
}

impl fmt::Display for RenderError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Error: ")?;
        match self {
			Self::Parser(err) => write!(f, "{err}"),
			Self::Lexer(err) => write!(f, "{err}"),
			Self::Io(err) => write!(f, "{err}"),
			Self::Fmt(err) => write!(f, "{err}"),
			Self::ArgumentParse(arg) => write!(f, "can't parse argument `{arg}` into integer"),
			Self::UndefinedSymbol(symbol, loc)
			=> write!(f, "undefined symbol {symbol} {loc}"),
			Self::Redefinition(symbol, loc)
			=> write!(f, "redefintion of symbol {symbol} {loc}"),
			Self::ListDestructure(loc)
			=> write!(f, "can't destructure a non list value {loc}"),
			Self::ListLength(expected, got, loc) => write!(f, "expected a list with {expected} elements but got a list with {got} elements {loc}"),
			Self::IntegerMatch(x, y, loc) => write!(f, "expected {x} but found {y} {loc}"),
			Self::IntegerMatchType(r#type, loc) => write!(f, "expected an integer but found {} {loc}", r#type),
			Self::Ruler(a, b, op, loc)
			=> write!(f, "ruler {op} operation performed between {a} and {b} instead of two points {loc}"),
			Self::Compass(a, b, loc)
			=> write!(f, "compass operation performed between {a} and {b} instead of a point and either a second point or pair of points {loc}"),
			Self::CompassElements(kind, index, loc)
			=> write!(f, "compass arc operation expects a list of two points but element {index} is a {kind} {loc}"),
			Self::CompassLength(size, loc)
			=> write!(f, "compass arc operation expects two elements but {size} was given {loc}"),
			Self::ArcPoints(loc)
			=> write!(f, "points for arc construction are not equidistant to the center {loc}"),
			Self::Intersection(a, b, loc)
			=> write!(f, "intersection operation performed between {a} and {b} instead of two elements {loc}"),
			Self::NoIntersection(loc) => write!(f, "No intersection found {loc}"),
			Self::QualificationValue(r#type, loc)
			=> write!(f, "left hand side of qualifications should be a list of points but a {} was found {loc}", r#type),
			Self::QualificationElements(kind, index, loc)
			=> write!(f, "left hand side of qualification should be a list of points but element {index} is a {kind} {loc}"),
			Self::QualificationOrigin(kind, loc)
			=> write!(f, "right hand side of qualification should be a point but a {kind} was found {loc}"),
			Self::QualificationEmpty(loc) => write!(f, "qualification has empty tuple of points {loc}"),
			Self::Callee(r#type, loc) => write!(f, "call of {} value {loc}", r#type),
			Self::CallSignature(name, loc) => write!(f, "call of {name} does not match any signature of the function {loc}"),
			Self::IndexOOB(loc) => write!(f, "index out of bounds {loc}"),
			Self::IndexTarget(r#type, loc) => write!(f, "index called on {} instead of list {loc}", r#type),
			Self::Index(r#type, loc) => write!(f, "index called with {} instead of integer {loc}", r#type),
			Self::Prepend(r#type, loc) => write!(f, "prepend called on {} instead of list {loc}", r#type),
			Self::Concat(r#type, side, loc) => write!(f, "concat expected a list on the {side} side but got {} {loc}", r#type),
			Self::Numeric(r#type, side, loc) => write!(f, "while evaluating numeric operation the {side} side was {} {loc}", r#type)
		}
    }
}

impl Error for RenderError {}

impl From<ParserError> for RenderError {
    fn from(err: ParserError) -> Self {
        match err {
            ParserError::Lexer(err) => Self::Lexer(err),
            err => Self::Parser(err),
        }
    }
}

impl From<io::Error> for RenderError {
    fn from(err: io::Error) -> Self {
        Self::Io(err)
    }
}

impl From<fmt::Error> for RenderError {
    fn from(err: fmt::Error) -> Self {
        Self::Fmt(err)
    }
}

struct Context<'a> {
    global_scope: &'a Scope,
    value_stack: Vec<Value>,
    evaluation_stack: Vec<Evaluation<'a>>,
    scope_stack: Vec<Scope>,
    list_len_stack: Vec<usize>,
}

impl<'a> Context<'a> {
    fn new(ast: &'a Ast, global_scope: &'a Scope) -> Self {
        Self {
            global_scope,
            value_stack: Vec::new(),
            evaluation_stack: vec![Evaluation::Expression(vec![(ast, false)])],
            scope_stack: Vec::new(),
            list_len_stack: Vec::new(),
        }
    }

    fn get_symbol(&self, symbol: &String, loc: Loc) -> Result<Value, RenderError> {
        if let Some(value) = self
            .scope_stack
            .last()
            .and_then(|s| s.get(symbol))
            .or_else(|| self.global_scope.get(symbol))
        {
            Ok(value.clone())
        } else {
            Err(RenderError::UndefinedSymbol(symbol.clone(), loc))
        }
    }
}

fn truncate_line(line: Line, viewbox: &[Element; 4]) -> Option<Element> {
    let line = Element::Line(line);
    let mut intersections = viewbox
        .iter()
        .map(|edge| edge & &line)
        .reduce(|mut a, mut b| {
            a.append(&mut b);
            a
        })
        .unwrap()
        .into_iter();
    match (intersections.next(), intersections.next()) {
        (Some(Element::Point(a)), Some(Element::Point(b))) => Some(Element::line_segment(a, b)),
        (Some(segment @ Element::LineSegment(_, _, _)), None) => Some(segment),
        _ => None,
    }
}

fn eval_call(name: &String, loc: Loc, context: &mut Context) -> Result<(), RenderError> {
    let stack_len = context.value_stack.len();
    let arguments = context
        .value_stack
        .split_off(stack_len - context.list_len_stack.pop().unwrap());
    let (bindings, expression, defs) = match context.get_symbol(name, loc)? {
        // This is ok because functions aren't mutated/dropped amid the evaluation of a single expression
        // The local scope vec may gain new elements but that's ok because functions are behind Rc
        Value::Function(callee) => unsafe {
            if let Some(def) = callee.find_signature(&arguments) {
                def
            } else {
                return Err(RenderError::CallSignature(name.clone(), loc));
            }
        },
        callee => return Err(RenderError::Callee(callee.r#type(), loc)),
    };
    let mut scope = HashMap::new();
    for (binding, argument) in zip(bindings.iter(), arguments.into_iter()) {
        argument.bind(binding, &mut scope)?;
    }
    // Tail call optimisation
    if let Some(Evaluation::Expression(ast_stack)) = context.evaluation_stack.last() {
        if ast_stack.last().is_none() {
            context.evaluation_stack.pop();
            context.scope_stack.pop();
        }
    }
    context.scope_stack.push(scope);
    context
        .evaluation_stack
        .push(Evaluation::Expression(vec![(expression, false)]));
    for (binding, expression) in defs.iter().rev() {
        context
            .evaluation_stack
            .push(Evaluation::Definition(binding, vec![(expression, false)]));
    }
    Ok(())
}

fn eval_op(op: Op, loc: Loc, context: &mut Context) -> Result<(), RenderError> {
    let a = context.value_stack.pop().unwrap();
    let b = context.value_stack.pop().unwrap();
    let value = match (op, a, b) {
        (Op::Line, Value::Element(Element::Point(a)), Value::Element(Element::Point(b))) => {
            Value::Element(Element::line(a, b))
        }
        (Op::LineSegment, Value::Element(Element::Point(a)), Value::Element(Element::Point(b))) => {
            Value::Element(Element::line_segment(a, b))
        }
        (Op::MidPoint, Value::Element(Element::Point(a)), Value::Element(Element::Point(b))) => {
            Value::Element(Element::Point(Point::new(
                (a.x + b.x) / Number::TWO,
                (a.y + b.y) / Number::TWO,
            )))
        }
        (
            Op::PerpindicularBisector,
            Value::Element(Element::Point(a)),
            Value::Element(Element::Point(b)),
        ) => {
            let (m, c) = if a.y == b.y {
                (None, (a.x + b.x) / Number::TWO)
            } else {
                let m = -(a.x - b.x) / (a.y - b.y);
                let c = (a.y + b.y) / Number::TWO - m * (a.x + b.x) / Number::TWO;
                (Some(m), c)
            };
            Value::Element(Element::Line(Line(m, c)))
        }
        (
            Op::Line | Op::LineSegment | Op::MidPoint | Op::PerpindicularBisector,
            Value::Element(a),
            Value::Element(b),
        ) => return Err(RenderError::Ruler(a.kind(), b.kind(), op, loc)),
        (Op::Line | Op::LineSegment | Op::MidPoint | Op::PerpindicularBisector, a, b) => {
            return Err(RenderError::Ruler(a.r#type(), b.r#type(), op, loc))
        }
        (Op::Circle, Value::Element(Element::Point(a)), Value::Element(Element::Point(b))) => {
            Value::Element(Element::circle(a, b))
        }
        (Op::Circle, Value::Element(Element::Point(c)), Value::List(elements)) => {
            if elements.len() != 2 {
                return Err(RenderError::CompassLength(elements.len(), loc));
            }
            let mut points = [None, None];
            for (i, v) in elements.into_iter().enumerate() {
                match v {
                    Value::Element(Element::Point(p)) => points[i] = Some(p),
                    Value::Element(e) => {
                        return Err(RenderError::CompassElements(e.kind(), i, loc))
                    }
                    v => return Err(RenderError::CompassElements(v.r#type(), i, loc)),
                }
            }
            let a = points[0].unwrap();
            let b = points[1].unwrap();
            if a.dist_sq(c) != b.dist_sq(c) {
                return Err(RenderError::ArcPoints(loc));
            }
            Value::Element(Element::arc(c, a, b))
        }
        (Op::Circle, Value::Element(a), b) => {
            return Err(RenderError::Compass(a.kind(), b.r#type(), loc))
        }
        (Op::Circle, a, b) => return Err(RenderError::Compass(a.r#type(), b.r#type(), loc)),
        (Op::Intersection, Value::Element(a), Value::Element(b)) => {
            let result = &a & &b;
            match result.len() {
                0 => return Err(RenderError::NoIntersection(loc)),
                1 => Value::Element(result.into_iter().next().unwrap()),
                _ => Value::List(List::from_vec(
                    result.into_iter().map(Value::Element).collect(),
                )),
            }
        }
        (Op::Intersection, a, b) => {
            return Err(RenderError::Intersection(a.r#type(), b.r#type(), loc))
        }
        (
            op @ (Op::Closest | Op::Furthest),
            Value::List(list),
            Value::Element(Element::Point(origin)),
        ) => {
            if list.len() == 0 {
                return Err(RenderError::QualificationEmpty(loc));
            }
            let mut extrema: Option<(Number, Point)> = None;
            for (i, v) in list.into_iter().enumerate() {
                match v {
                    Value::Element(Element::Point(p)) => {
                        let d = p.dist_sq(origin);
                        if extrema.is_none()
                            || (matches!(op, Op::Closest) && d < extrema.as_ref().unwrap().0)
                            || (matches!(op, Op::Furthest) && d > extrema.as_ref().unwrap().0)
                        {
                            extrema = Some((d, p))
                        }
                    }
                    Value::Element(e) => {
                        return Err(RenderError::QualificationElements(e.kind(), i, loc))
                    }
                    v => return Err(RenderError::QualificationElements(v.r#type(), i, loc)),
                }
            }
            Value::Element(Element::Point(extrema.unwrap().1))
        }
        (Op::Closest | Op::Furthest, Value::List(_), Value::Element(e)) => {
            return Err(RenderError::QualificationOrigin(e.kind(), loc))
        }
        (Op::Closest | Op::Furthest, Value::List(_), value) => {
            return Err(RenderError::QualificationOrigin(value.r#type(), loc))
        }
        (Op::Closest | Op::Furthest, value, _) => {
            return Err(RenderError::QualificationValue(value.r#type(), loc))
        }
        (
            op @ (Op::Add | Op::Sub | Op::Mul | Op::Div | Op::Mod),
            Value::Integer(n),
            Value::Integer(m),
        ) => match op {
            Op::Add => Value::Integer(n + m),
            Op::Sub => Value::Integer(n - m),
            Op::Mul => Value::Integer(n * m),
            Op::Div => Value::Integer(n / m),
            Op::Mod => Value::Integer(n % m),
            _ => unreachable!(),
        },
        (Op::Add | Op::Sub | Op::Mul | Op::Div | Op::Mod, Value::Integer(_), value) => {
            return Err(RenderError::Numeric(value.r#type(), "right", loc))
        }
        (Op::Add | Op::Sub | Op::Mul | Op::Div | Op::Mod, value, _) => {
            return Err(RenderError::Numeric(value.r#type(), "left", loc))
        }
        (Op::Index, Value::List(list), Value::Integer(i)) => {
            if i < 0 || i as usize >= list.len() {
                return Err(RenderError::IndexOOB(loc));
            }
            list[i as usize].clone()
        }
        (Op::Index, Value::List(_), value) => return Err(RenderError::Index(value.r#type(), loc)),
        (Op::Index, value, _) => return Err(RenderError::IndexTarget(value.r#type(), loc)),
        (Op::Prepend, value, Value::List(mut list)) => {
            list.push(value);
            Value::List(list)
        }
        (Op::Prepend, _, value) => return Err(RenderError::Prepend(value.r#type(), loc)),
        (Op::Concat, Value::List(left), Value::List(mut right)) => {
            right.prepend(left);
            Value::List(right)
        }
        (Op::Concat, Value::List(_), value) => {
            return Err(RenderError::Concat(value.r#type(), "right", loc))
        }
        (Op::Concat, value, _) => return Err(RenderError::Concat(value.r#type(), "left", loc)),
    };
    context.value_stack.push(value);
    Ok(())
}

fn eval(ast: &Ast, global_scope: &Scope) -> Result<Value, RenderError> {
    let mut context = Context::new(ast, global_scope);
    loop {
        let ((ast, visited), ast_stack) = match context.evaluation_stack.last_mut() {
            Some(Evaluation::Expression(nodes)) => {
                if let Some(node) = nodes.pop() {
                    (node, nodes)
                } else {
                    context.evaluation_stack.pop();
                    context.scope_stack.pop();
                    continue;
                }
            }
            Some(Evaluation::ListItem(nodes)) => {
                if let Some(node) = nodes.pop() {
                    (node, nodes)
                } else {
                    context.evaluation_stack.pop();
                    continue;
                }
            }
            Some(Evaluation::Argument(nodes)) => {
                if let Some(node) = nodes.pop() {
                    (node, nodes)
                } else {
                    context.evaluation_stack.pop();
                    continue;
                }
            }
            Some(Evaluation::Definition(binding, nodes)) => {
                if let Some(node) = nodes.pop() {
                    (node, nodes)
                } else {
                    context
                        .value_stack
                        .pop()
                        .unwrap()
                        .bind(binding, context.scope_stack.last_mut().unwrap())?;
                    context.evaluation_stack.pop();
                    continue;
                }
            }
            None => break,
        };
        let Ast(node, loc) = ast;
        let loc = *loc;
        match (node, visited) {
            (AstNode::Symbol(symbol), _) => {
                let value = context.get_symbol(symbol, loc)?;
                context.value_stack.push(value);
            }
            (AstNode::Integer(n), _) => context.value_stack.push(Value::Integer(*n)),
            (AstNode::List(items), false) => {
                ast_stack.push((ast, true));
                context.list_len_stack.push(items.len());
                context.evaluation_stack.extend(
                    items
                        .iter()
                        .rev()
                        .map(|ast| Evaluation::ListItem(vec![(ast, false)])),
                );
            }
            (AstNode::List(_), true) => {
                let stack_len = context.value_stack.len();
                let list = List::from_vec(
                    context
                        .value_stack
                        .split_off(stack_len - context.list_len_stack.pop().unwrap()),
                );
                context.value_stack.push(Value::List(list));
            }
            (AstNode::Call(_, arguments), false) => {
                ast_stack.push((ast, true));
                context.list_len_stack.push(arguments.len());
                context.evaluation_stack.extend(
                    arguments
                        .iter()
                        .rev()
                        .map(|ast| Evaluation::Argument(vec![(ast, false)])),
                );
            }
            (AstNode::Call(name, _), true) => eval_call(name, loc, &mut context)?,
            (AstNode::Op(_, lhs, rhs), false) => {
                ast_stack.push((ast, true));
                ast_stack.push((lhs, false));
                ast_stack.push((rhs, false));
            }
            (&AstNode::Op(op, _, _), true) => eval_op(op, loc, &mut context)?,
        }
    }
    Ok(context.value_stack.pop().unwrap())
}

pub fn render(i: String, o: &mut dyn Write, args: Vec<String>) -> Result<(), RenderError> {
    let program = parse(i)?;
    let c1 = Point::new(Number::ONE, Number::ZERO);
    let c2 = Point::new(Number::ZERO, Number::ZERO);
    let c3 = Point::new(Number::ZERO, Number::ONE);
    let c4 = Point::new(Number::ONE, Number::ONE);
    let mut global_scope: Scope = [("c1", c1), ("c2", c2), ("c3", c3), ("c4", c4)]
        .into_iter()
        .map(|(k, v)| (k.to_string(), Value::Element(Element::Point(v))))
        .collect();
    let mut parsed = Vec::new();
    for arg in args.into_iter().rev() {
        parsed.push(Value::Integer(match arg.parse::<isize>() {
            Ok(i) => i,
            Err(_) => return Err(RenderError::ArgumentParse(arg)),
        }));
    }
    global_scope.insert("args".to_owned(), Value::List(List::from_vec(parsed)));
    let viewbox = [
        Element::line_segment(c1, c2),
        Element::line_segment(c2, c3),
        Element::line_segment(c3, c4),
        Element::line_segment(c4, c1),
    ];
    for FunctionStatement(name, signature, body, defs, loc) in program.functions {
        if let Some(def) = global_scope.get_mut(&name) {
            match def {
                Value::Function(f) => f.add_definition(signature, body, defs),
                _ => return Err(RenderError::Redefinition(name.clone(), loc)),
            }
        } else {
            Value::Function(Function::new(signature, body, defs))
                .bind(&Binding::Name(name, loc), &mut global_scope)?;
        }
    }
    for AssignmentStatement(binding, expression) in program.assignments {
        let value = eval(&expression, &global_scope)?;
        value.bind(&binding, &mut global_scope)?;
    }
    let mut draws = Vec::new();
    for DrawStatement(style, expression) in program.draws {
        let values = eval(&expression, &global_scope)?.flatten();
        let mut elements = Vec::with_capacity(values.len());
        for value in values {
            match value {
                Value::Element(Element::Point(_)) if style.fill.is_some() => (),
                Value::Element(Element::Line(line)) => {
                    if let Some(e) = truncate_line(line, &viewbox) {
                        elements.push(e);
                    }
                }
                Value::Element(e) => elements.push(e),
                _ => (),
            }
        }
        if !elements.is_empty() {
            draws.push((style, elements))
        }
    }
    svg(draws, o)
}
