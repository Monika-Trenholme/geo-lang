use super::lexer::{Lexeme, Lexer, LexerError, Loc, Op, Token};
use std::{error::Error, fmt, iter::Peekable};

pub enum Binding {
    Underscore(Loc),
    Name(String, Loc),
    Integer(isize, Loc),
    List(Vec<Binding>, Loc),
    Pop(Vec<Binding>, Box<Binding>, Loc),
}

pub enum AstNode {
    Symbol(String),
    Integer(isize),
    Op(Op, Box<Ast>, Box<Ast>),
    List(Vec<Ast>),
    Call(String, Vec<Ast>),
}

pub struct Ast(pub AstNode, pub Loc);

pub type Signature = Vec<Binding>;
pub type Definition = (Binding, Ast);

#[derive(Clone)]
pub struct Style {
    pub fill: Option<String>,
    pub stroke: Option<String>,
    pub width: Option<String>,
}

impl fmt::Display for Style {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "fill=\"{}\"", self.fill.as_deref().unwrap_or("none"))?;
        if let Some(stroke) = self.stroke.as_ref() {
            write!(f, " stroke=\"{stroke}\"")?;
        }
        if let Some(width) = self.width.as_ref() {
            write!(f, " stroke-width=\"{width}\"")?;
        }
        Ok(())
    }
}

pub struct AssignmentStatement(pub Binding, pub Ast);

pub struct FunctionStatement(
    pub String,
    pub Signature,
    pub Ast,
    pub Vec<Definition>,
    pub Loc,
);

pub struct DrawStatement(pub Style, pub Ast);

enum Statement {
    Assignment(AssignmentStatement),
    Function(FunctionStatement),
    Draw(DrawStatement),
}

pub struct Program {
    pub functions: Vec<FunctionStatement>,
    pub assignments: Vec<AssignmentStatement>,
    pub draws: Vec<DrawStatement>,
}

#[derive(Debug)]
pub enum ParserError {
    Lexer(LexerError),
    UnexpectedEnd,
    UnexpectedToken(Token),
    Expected(&'static str, Token),
}

impl fmt::Display for ParserError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Lexer(err) => write!(f, "{err}"),
            Self::UnexpectedEnd => write!(f, "Unexpected end of input"),
            Self::UnexpectedToken(token) => write!(f, "Unexpected token {token}"),
            Self::Expected(expected, token) => write!(f, "Expected {expected} but found {token}"),
        }
    }
}

impl Error for ParserError {}

impl From<LexerError> for ParserError {
    fn from(err: LexerError) -> Self {
        Self::Lexer(err)
    }
}

fn peek(lexer: &mut Peekable<Lexer>) -> Result<Option<&Token>, LexerError> {
    match lexer.peek() {
        Some(Ok(token)) => Ok(Some(token)),
        Some(Err(err)) => Err(err.clone()),
        None => Ok(None),
    }
}

fn eat(lexer: &mut Peekable<Lexer>) -> Result<Token, ParserError> {
    match lexer.next() {
        Some(result) => Ok(result?),
        None => Err(ParserError::UnexpectedEnd),
    }
}

fn call(lexer: &mut Peekable<Lexer>, name: String, loc: Loc) -> Result<Ast, ParserError> {
    eat(lexer)?;
    let mut arguments = Vec::new();
    loop {
        if matches!(peek(lexer)?, Some(Token(Lexeme::RParen, _))) {
            eat(lexer)?;
            break;
        }
        arguments.push(expression(lexer)?);
        if matches!(peek(lexer)?, Some(Token(Lexeme::Seperator, _))) {
            eat(lexer)?;
        }
    }
    Ok(Ast(AstNode::Call(name, arguments), loc))
}

fn list(lexer: &mut Peekable<Lexer>, loc: Loc) -> Result<Ast, ParserError> {
    let mut items = Vec::new();
    loop {
        if matches!(peek(lexer)?, Some(Token(Lexeme::RBracket, _))) {
            eat(lexer)?;
            break;
        }
        items.push(expression(lexer)?);
        if matches!(peek(lexer)?, Some(Token(Lexeme::Seperator, _))) {
            eat(lexer)?;
        }
    }
    Ok(Ast(AstNode::List(items), loc))
}

fn atom(lexer: &mut Peekable<Lexer>) -> Result<Ast, ParserError> {
    match eat(lexer)? {
        Token(Lexeme::Symbol(name), loc) => match peek(lexer)? {
            Some(Token(Lexeme::LParen, _)) => call(lexer, name, loc),
            _ => Ok(Ast(AstNode::Symbol(name), loc)),
        },
        Token(Lexeme::Integer(n), loc) => Ok(Ast(AstNode::Integer(n), loc)),
        Token(Lexeme::LBracket, loc) => list(lexer, loc),
        Token(Lexeme::LParen, _) => {
            let atom = expression(lexer)?;
            match eat(lexer)? {
                Token(Lexeme::RParen, _) => Ok(atom),
                token => Err(ParserError::UnexpectedToken(token)),
            }
        }
        token => Err(ParserError::UnexpectedToken(token)),
    }
}

fn expression(lexer: &mut Peekable<Lexer>) -> Result<Ast, ParserError> {
    let mut operands = Vec::new();
    let mut operators = Vec::<(Op, Loc)>::new();
    let mut last_was_operator = true;
    loop {
        if matches!(
            peek(lexer)?,
            Some(Token(
                Lexeme::Seperator
                    | Lexeme::RParen
                    | Lexeme::RBracket
                    | Lexeme::Terminator
                    | Lexeme::LBrace,
                _,
            ))
        ) {
            if last_was_operator {
                return Err(ParserError::UnexpectedToken(eat(lexer)?));
            }
            break;
        }
        match peek(lexer)? {
            Some(&Token(Lexeme::Op(op), loc)) if last_was_operator => {
                return Err(ParserError::UnexpectedToken(Token(Lexeme::Op(op), loc)));
            }
            Some(&Token(Lexeme::Op(op), loc)) => {
                eat(lexer)?;
                let current = op.precedence();
                let right = op.is_right_associative();
                loop {
                    let top = operators.last().map(|(op, _)| op.precedence()).unwrap_or(0);
                    if !right && current <= top || right && current < top {
                        let rhs = operands.pop().unwrap();
                        let lhs = operands.pop().unwrap();
                        let (op, loc) = operators.pop().unwrap();
                        operands.push(Ast(AstNode::Op(op, Box::new(lhs), Box::new(rhs)), loc));
                    } else {
                        break;
                    }
                }
                operators.push((op, loc));
                last_was_operator = true;
            }
            _ if !last_was_operator => return Err(ParserError::UnexpectedToken(eat(lexer)?)),
            _ => {
                operands.push(atom(lexer)?);
                last_was_operator = false;
            }
        }
    }

    for (op, loc) in operators.into_iter().rev() {
        let rhs = operands.pop().unwrap();
        let lhs = operands.pop().unwrap();
        operands.push(Ast(AstNode::Op(op, Box::new(lhs), Box::new(rhs)), loc));
    }

    Ok(operands.pop().unwrap())
}

fn binding_list(lexer: &mut Peekable<Lexer>, loc: Loc) -> Result<Binding, ParserError> {
    if let Some(Token(Lexeme::RBracket, _)) = peek(lexer)? {
        eat(lexer)?;
        return Ok(Binding::List(Vec::new(), loc));
    }
    let mut bindings = Vec::new();
    loop {
        bindings.push(binding(lexer)?);
        match eat(lexer)? {
            Token(Lexeme::Seperator, _) => continue,
            Token(Lexeme::RBracket, _) => break,
            token => return Err(ParserError::Expected(", or ]", token)),
        }
    }
    Ok(Binding::List(bindings, loc))
}

fn binding_atom(lexer: &mut Peekable<Lexer>) -> Result<Binding, ParserError> {
    match eat(lexer)? {
        Token(Lexeme::Underscore, loc) => Ok(Binding::Underscore(loc)),
        Token(Lexeme::Symbol(name), loc) => Ok(Binding::Name(name, loc)),
        Token(Lexeme::Integer(n), loc) => Ok(Binding::Integer(n, loc)),
        Token(Lexeme::LBracket, loc) => binding_list(lexer, loc),
        token => Err(ParserError::UnexpectedToken(token)),
    }
}

fn binding(lexer: &mut Peekable<Lexer>) -> Result<Binding, ParserError> {
    let atom = binding_atom(lexer)?;
    let loc = if let Some(Token(Lexeme::Op(Op::Prepend), loc)) = peek(lexer)? {
        *loc
    } else {
        return Ok(atom);
    };
    let mut bindings = vec![atom];
    while let Some(Token(Lexeme::Op(Op::Prepend), _)) = peek(lexer)? {
        eat(lexer)?;
        bindings.push(binding_atom(lexer)?);
    }
    let rest = bindings.pop().unwrap();
    Ok(Binding::Pop(bindings, Box::new(rest), loc))
}

fn stroke_statement(lexer: &mut Peekable<Lexer>) -> Result<DrawStatement, ParserError> {
    eat(lexer)?;
    let (colour, width) = match (eat(lexer)?, eat(lexer)?) {
        (Token(Lexeme::String(colour), _), Token(Lexeme::String(width), _)) => (colour, width),
        (Token(Lexeme::String(_), _), token) | (token, _) => {
            return Err(ParserError::Expected("string", token))
        }
    };
    Ok(DrawStatement(
        Style {
            fill: None,
            stroke: Some(colour),
            width: Some(width),
        },
        expression(lexer)?,
    ))
}

fn fill_statement(lexer: &mut Peekable<Lexer>) -> Result<DrawStatement, ParserError> {
    eat(lexer)?;
    let colour = match eat(lexer)? {
        Token(Lexeme::String(colour), _) => colour,
        token => return Err(ParserError::Expected("string", token)),
    };
    Ok(DrawStatement(
        Style {
            fill: Some(colour),
            stroke: None,
            width: None,
        },
        expression(lexer)?,
    ))
}

fn function_statement(
    lexer: &mut Peekable<Lexer>,
    name: String,
    loc: Loc,
) -> Result<FunctionStatement, ParserError> {
    let mut parameters = Vec::new();
    loop {
        if matches!(peek(lexer)?, Some(Token(Lexeme::RParen, _))) {
            eat(lexer)?;
            break;
        }
        parameters.push(binding(lexer)?);
        match eat(lexer)? {
            Token(Lexeme::Seperator, _) => (),
            Token(Lexeme::RParen, _) => break,
            token => return Err(ParserError::Expected(", or )", token)),
        }
    }
    match eat(lexer)? {
        Token(Lexeme::Equals, _) => (),
        token => return Err(ParserError::Expected("=", token)),
    }
    let body = expression(lexer)?;
    if !matches!(peek(lexer)?, Some(Token(Lexeme::LBrace, _))) {
        return Ok(FunctionStatement(name, parameters, body, Vec::new(), loc));
    }
    eat(lexer)?;
    let mut defs = Vec::new();
    loop {
        let binding = binding(lexer)?;
        match eat(lexer)? {
            Token(Lexeme::Equals, _) => (),
            token => return Err(ParserError::Expected("=", token)),
        }
        let body = expression(lexer)?;
        match eat(lexer)? {
            Token(Lexeme::Terminator, _) => (),
            token => return Err(ParserError::UnexpectedToken(token)),
        }
        defs.push((binding, body));
        if matches!(peek(lexer)?, Some(Token(Lexeme::RBrace, _))) {
            eat(lexer)?;
            break;
        }
    }
    Ok(FunctionStatement(name, parameters, body, defs, loc))
}

fn statement(lexer: &mut Peekable<Lexer>) -> Result<Statement, ParserError> {
    let statement = match peek(lexer)?.unwrap() {
        &Token(Lexeme::Stroke, _) => Statement::Draw(stroke_statement(lexer)?),
        &Token(Lexeme::Fill, _) => Statement::Draw(fill_statement(lexer)?),
        Token(Lexeme::Symbol(name), loc) => {
            let name = name.clone();
            let loc = *loc;
            eat(lexer)?;
            match eat(lexer)? {
                Token(Lexeme::Equals, _) => Statement::Assignment(AssignmentStatement(
                    Binding::Name(name, loc),
                    expression(lexer)?,
                )),
                Token(Lexeme::LParen, _) => {
                    Statement::Function(function_statement(lexer, name, loc)?)
                }
                token => return Err(ParserError::Expected("=", token)),
            }
        }
        _ => {
            let binding = binding(lexer)?;
            match eat(lexer)? {
                Token(Lexeme::Equals, _) => {
                    Statement::Assignment(AssignmentStatement(binding, expression(lexer)?))
                }
                token => return Err(ParserError::Expected("=", token)),
            }
        }
    };

    match eat(lexer)? {
        Token(Lexeme::Terminator, _) => Ok(statement),
        token => Err(ParserError::UnexpectedToken(token)),
    }
}

pub fn parse(input: String) -> Result<Program, ParserError> {
    let mut lexer = Lexer::new(input).peekable();
    let mut functions = Vec::new();
    let mut assignments = Vec::new();
    let mut draws = Vec::new();
    while lexer.peek().is_some() {
        match statement(&mut lexer)? {
            Statement::Function(function) => functions.push(function),
            Statement::Assignment(assignment) => assignments.push(assignment),
            Statement::Draw(draw) => draws.push(draw),
        }
    }
    Ok(Program {
        functions,
        assignments,
        draws,
    })
}
