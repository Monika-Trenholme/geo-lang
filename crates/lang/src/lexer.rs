use std::{error::Error, fmt, iter::Peekable};

#[derive(Clone, Copy, Debug)]
pub struct Loc(pub usize, pub usize);

impl fmt::Display for Loc {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "[{}:{}]", self.0, self.1)
    }
}

#[derive(Clone, Copy, Debug)]
pub enum Op {
    Line,
    LineSegment,
    Circle,
    MidPoint,
    PerpindicularBisector,
    Intersection,
    Closest,
    Furthest,
    Add,
    Sub,
    Mul,
    Div,
    Mod,
    Index,
    Prepend,
    Concat,
}

#[derive(Debug)]
pub enum Lexeme {
    Stroke,
    Fill,
    String(String),
    Symbol(String),
    Integer(isize),
    Underscore,
    Op(Op),
    Seperator,
    LParen,
    RParen,
    LBracket,
    RBracket,
    Equals,
    LBrace,
    RBrace,
    Terminator,
}

impl fmt::Display for Op {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Op::Line => "<->",
                Op::LineSegment => "--",
                Op::Circle => "@",
                Op::MidPoint => "-*-",
                Op::PerpindicularBisector => "-|-",
                Op::Intersection => "&",
                Op::Closest => ">",
                Op::Furthest => "<",
                Op::Add => "+",
                Op::Sub => "-",
                Op::Mul => "*",
                Op::Div => "/",
                Op::Mod => "%",
                Op::Index => "!!",
                Op::Prepend => ":",
                Op::Concat => "++",
            }
        )
    }
}

impl Op {
    pub fn precedence(&self) -> u8 {
        match self {
            Op::Index => 7,
            Op::Line => 6,
            Op::LineSegment => 6,
            Op::Circle => 6,
            Op::MidPoint => 6,
            Op::PerpindicularBisector => 6,
            Op::Intersection => 5,
            Op::Closest => 4,
            Op::Furthest => 4,
            Op::Prepend => 3,
            Op::Concat => 3,
            Op::Mul => 2,
            Op::Div => 2,
            Op::Mod => 2,
            Op::Add => 1,
            Op::Sub => 1,
        }
    }

    pub fn is_right_associative(&self) -> bool {
        matches!(self, Op::Prepend)
    }
}

impl fmt::Display for Lexeme {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Stroke => write!(f, "stroke"),
            Self::Fill => write!(f, "fill"),
            Self::String(string) => write!(f, "\"{string}\""),
            Self::Symbol(symbol) => write!(f, "{symbol}"),
            Self::Integer(n) => write!(f, "{n}"),
            Self::Underscore => write!(f, "_"),
            Self::Op(op) => write!(f, "{op}"),
            Self::Seperator => write!(f, ","),
            Self::LParen => write!(f, "("),
            Self::RParen => write!(f, ")"),
            Self::LBracket => write!(f, "["),
            Self::RBracket => write!(f, "]"),
            Self::Equals => write!(f, "="),
            Self::LBrace => write!(f, "{{"),
            Self::RBrace => write!(f, "}}"),
            Self::Terminator => write!(f, ";"),
        }
    }
}

#[derive(Debug)]
pub struct Token(pub Lexeme, pub Loc);

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} {}", self.0, self.1)
    }
}

#[derive(Clone, Debug)]
pub enum LexerError {
    UnterminatedString(Loc),
    UnknownOp(String, Loc),
}

impl fmt::Display for LexerError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::UnterminatedString(loc) => write!(f, "Unterminated string starting at {loc}"),
            Self::UnknownOp(op, loc) => write!(f, "Unknown operator {op} {loc}"),
        }
    }
}

impl Error for LexerError {}

pub struct Lexer {
    chars: Peekable<std::vec::IntoIter<char>>,
    loc: Loc,
}

const TERM_CHARS: &str = "\n[](){},:;";

impl Lexer {
    pub fn new(source: String) -> Self {
        Self {
            chars: source.chars().collect::<Vec<char>>().into_iter().peekable(),
            loc: Loc(1, 1),
        }
    }

    pub fn eat(&mut self) -> Option<char> {
        match self.chars.next() {
            Some('\n') => {
                self.loc.0 += 1;
                self.loc.1 = 1;
                Some('\n')
            }
            Some(c) => {
                self.loc.1 += 1;
                Some(c)
            }
            None => None,
        }
    }

    pub fn eat_if<P>(&mut self, predicate: P) -> Option<char>
    where
        P: FnOnce(char) -> bool,
    {
        if self.chars.peek().cloned().map_or(false, predicate) {
            self.eat()
        } else {
            None
        }
    }

    pub fn string(&mut self, start_loc: Loc) -> Result<Lexeme, LexerError> {
        let mut string = String::new();
        while let Some(c) = self.eat_if(|c| c != '"') {
            string.push(c);
        }
        if self.eat().is_none() {
            return Err(LexerError::UnterminatedString(start_loc));
        }
        Ok(Lexeme::String(string))
    }

    pub fn integer(&mut self, c: char) -> Result<Lexeme, LexerError> {
        let mut n = c as isize - '0' as isize;
        while let Some(c) = self.eat_if(|c| c.is_ascii_digit()) {
            n = 10 * n + (c as isize - '0' as isize);
        }
        Ok(Lexeme::Integer(n))
    }

    pub fn symbol(&mut self, c: char) -> Result<Lexeme, LexerError> {
        let mut symbol = String::new();
        symbol.push(c);
        while let Some(c) = self.eat_if(|c| c.is_alphanumeric()) {
            symbol.push(c);
        }
        Ok(match symbol.as_str() {
            "stroke" => Lexeme::Stroke,
            "fill" => Lexeme::Fill,
            _ => Lexeme::Symbol(symbol),
        })
    }

    pub fn operator(&mut self, c: char, start_loc: Loc) -> Result<Lexeme, LexerError> {
        let mut operator = String::new();
        operator.push(c);
        while let Some(c) =
            self.eat_if(|c| !c.is_whitespace() && !TERM_CHARS.contains(c) && !c.is_alphanumeric())
        {
            operator.push(c);
        }
        Ok(Lexeme::Op(match operator.as_str() {
            "<->" => Op::Line,
            "--" => Op::LineSegment,
            "@" => Op::Circle,
            "-*-" => Op::MidPoint,
            "-|-" => Op::PerpindicularBisector,
            "&" => Op::Intersection,
            ">" => Op::Closest,
            "<" => Op::Furthest,
            "+" => Op::Add,
            "-" => Op::Sub,
            "*" => Op::Mul,
            "/" => Op::Div,
            "%" => Op::Mod,
            "!!" => Op::Index,
            ":" => Op::Prepend,
            "++" => Op::Concat,
            _ => return Err(LexerError::UnknownOp(operator, start_loc)),
        }))
    }
}

impl Iterator for Lexer {
    type Item = Result<Token, LexerError>;
    fn next(&mut self) -> Option<Self::Item> {
        while let Some(c) = self.eat_if(|c| c.is_whitespace() || c == '#') {
            if c == '#' {
                while self.eat_if(|c| c != '\n').is_some() {}
            }
        }
        let start_loc = self.loc;
        let lexeme = match self.eat()? {
            '"' => self.string(start_loc),
            c @ '0'..='9' => self.integer(c),
            c if c.is_alphabetic() => self.symbol(c),
            ',' => Ok(Lexeme::Seperator),
            '(' => Ok(Lexeme::LParen),
            ')' => Ok(Lexeme::RParen),
            '[' => Ok(Lexeme::LBracket),
            ']' => Ok(Lexeme::RBracket),
            '=' => Ok(Lexeme::Equals),
            ';' => Ok(Lexeme::Terminator),
            '{' => Ok(Lexeme::LBrace),
            '}' => Ok(Lexeme::RBrace),
            '_' => Ok(Lexeme::Underscore),
            c => self.operator(c, start_loc),
        };
        Some(lexeme.map(|inner| Token(inner, start_loc)))
    }
}
