# Geometric Construction Language
This is my currently unnamed geometric construction language.
It allows for writing euclidean constructions concisely, and then rendering them to SVG.

## VSCode syntax highlighting
You can copy the syntax folder into your vscode extensions directory with any directory name of your choosing. Afterwards files with the extension .geo will have some simple syntax highlighting applied.

## CLI
```
# args are passed into the construction as a list of integers 'args',
# this can allow for eg. rendering frames of an animation
cargo run --bin cli -r -- path -- args
# rendered svg is saved to path.svg
```

## Web IDE
```
# Outputs WASM build to web-ide folder
make web-ide release=true
```
ctrl-r to render
ctrl-s to save

## Making a construction
Each construction is inscribed into a square, with the corners named c1, c2, c3, c4.
You can make lines
```
# infinite line
AB = A <-> B;
# line segment
CD = C -- D;
```
Circles and arcs
```
# Point A is the center, and point B defines the radius
C = A @ B;
# Arc with C as the center that goes counter clockwise from B to D
A = C @ [B, D];
```
And Find intersections
```
C = A @ B;
# One intersection
a = C & A -- B;
# Two intersections
[a, b] = C & A <-> B;
# Intersection closest to B, which would be B itself
B1 = [a, b] > B;
# Intersection furthest from B, which would be diametrically opposed to B
D = [a, b] < B;
```
There are also some convenience functions,
to shorten code and execution time.
```
# Midpoint between A and B
M = A -*- B;
# Create a perpindicular bisector between C and D
PB = C -|- D;
```
The line, circle, midpoint, and perpindicular bisector construction operators have highest precedence,
intersections evaluate left to right, and the furthest/closest operators have lowest precedence.
You can use parenthesis to control order of operations.

Lists
```
list = [d, e, f, g];
longerList = a : b : c : list;
longestList = longerList ++ [h, i, j];

zeroth = list !! 0;
first = list !! 1;
```

Functions
```
# A function f, that takes two parameters a and b,
# then returns c, which is defined as the midpoint of a and b
f(a, b) = c     # Main function body
{
	c = a -*- b # Subdefintions
};

# Recursion
mapRev(xs, f) = mapRev(xs, f, []);
mapRev(x:xs, f, ys) = mapRev(xs, f, f(y) : ys);
mapRev([], f, ys) = ys;
```
