C = cargo build --package

ifeq ($(release), true)
	RELEASE_FLAG = -r
	CANDIDATE = release
else
	CANDIDATE = debug
endif

cli:
	$(C) cli $(RELEASE_FLAG)

web-ide:
	$(C) $@ --target wasm32-unknown-unknown $(RELEASE_FLAG)
	$(CARGO_HOME)/bin/wasm-bindgen target/wasm32-unknown-unknown/$(CANDIDATE)/$@.wasm --target web --no-typescript --out-dir $@

clean:
	rm -f web-ide/web-ide_bg.wasm  web-ide/web-ide.js
	rm -f examples/*.svg examples/*.png

.PHONY: cli web-ide clean
