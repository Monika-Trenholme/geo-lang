let TextMode = ace.require("ace/mode/text").Mode;
let TextHighlight = (new TextMode()).HighlightRules;

class GeoHighlight extends TextHighlight {
    constructor() {
		super();
		this.$rules = {
			"start": [
				{
					token: "comment",
					regex: /#.*$/
				},
				{
					token: "keyword.other",
					regex: /(?:fill|stroke)\b/
				},
				{
					token: "string",
					regex: /"[^"]*(?:"|$)/,
				},
				{
					token: "constant.numeric",
					regex: /\b\d+\b/
				},
				{
					token: "entity.name.function",
					regex: /[a-zA-Z][a-zA-Z\d]*(?=\()/
				},
				{
					token: "vaiable.name",
					regex: /[a-zA-Z][a-zA-Z\d]*\b/
				},
				{
					token: "keyword.operator",
					regex: /[^a-zA-Z\d"\(\)\[\]\{\},;#]+/
				}
			]
		};
	}
}

class GeoMode extends TextMode {
	constructor() {
		super();
		this.lineCommentStart = "#";
		this.HighlightRules = GeoHighlight;
	}
}

let editor = ace.edit("input", {
    wrap: true,
    theme: "ace/theme/monokai",
    mode: new GeoMode()
});

export function get_code() {
    return editor.getValue();
}