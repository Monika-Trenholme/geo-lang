origin = c1 -- c3 & c2 -- c4; # Center of square

th = c1 -*- c2;               # Midpoint of top edge

c = origin @ th;              # Circle hexagon will be inscribed in

p2 = th;                      # Second point is top edge midpoint

secondary = p2 @ origin & c;  # Secondary points from circle intersections
p1 = secondary > c1;          # First point is closest to top right corner
p3 = secondary < c1;          # Third point counter clockwise
p4 = p3 @ origin & c < p2;    # Fourth point counter clockwise
p5 = p4 @ origin & c < p3;    # Fifth point counter clockwise
p6 = p5 @ origin & c < p4;    # Sixth point counter clockwise

stroke "blue" "1%" [          # Display hexagram
	p1 -- p3,                 # Odd triangle
	p3 -- p5,
	p5 -- p1,
	
	p2 -- p4,                 # Even triangle
	p4 -- p6,
	p6 -- p2
];
