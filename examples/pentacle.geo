origin = c1 -- c3 & c2 -- c4;                           # Center point of square

topEdgeMid = c1 -*- c2;                                 # Midpoint of top edge

c = origin @ topEdgeMid;                                # Circle that pentagram will be inscribed in

leftEdgeMid = c2 -*- c3;                                # Midpoint of left edge

halfLeft = leftEdgeMid -*- origin;                      # Half point between origin and left edge midpoint 

phiPoint = halfLeft @ origin & halfLeft -- topEdgeMid;  # Construct golden ratio

initial = topEdgeMid @ phiPoint & c;                    # Arc from top edge mid point into golden ratio point to form initial top two points
p1 = initial > c1;                                      # Point closest to top right corner
p2 = initial < c1;                                      # Second point counter clockwise
p3 = p2 @ p1 & c < p1;                                  # Third point counter clockwise
p4 = p3 @ p2 & c < p2;                                  # Fourth point counter clockwise
p5 = p4 @ p3 & c < p3;                                  # Fifth point counter clockwise

stroke "red" "1%" [                                     # Display pentacle
	c,
	p1 -- p3,
	p3 -- p5,
	p5 -- p2,
	p2 -- p4,
	p4 -- p1
];
