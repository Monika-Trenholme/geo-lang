topEdgeMid = c1 -*- c2;
bottomEdgeMid = c3 -*- c4;
o = topEdgeMid -*- bottomEdgeMid;
th = topEdgeMid -*- o;
tq = th -*- o;
te = tq -*- o;
bh = o -*- bottomEdgeMid;
bq = bh -*- o;
be = bq -*- bh;

c = te @ be;

t = c & (o -- (o <-> topEdgeMid & topEdgeMid @ o < o));
i = (t @ te) & c;
p1 = i < c1;
p2 = i > c1;
l = (p1 @ te) & c < t;
r = (p2 @ te) & c < t;

# Constructs array of edges from array of points
triangle([a, b, c]) = [a -- b, b -- c, c -- a];

# Create isocelese reflection point,
# given points a, b, and c, where c is opposing the line of reflection ab.
reflection([a, b, c]) = a @ center & b @ center < c {
	acp = a -|- c;      # Perpindicular bisector of ac
	bcp = b -|- c;      # Perpindicular bisector of bc
	center = acp & bcp; # Center of equilateral
};

# Creates the next reflection and equilateral in the series
step([a, b, c]) = [[a, b, r], [n, r, a]] {
	r = reflection([a, b, c]);
	n = a @ r & r @ a < c;
};

# Performs function on three inputs, returning three outputs
triple(f, [a, b, c]) = [f(a), f(b), f(c)];

# Draw three triangles
triangles(ts) = triple(triangle, ts);

# Perform steps on each side of the base triangle
steps(seeds) = [[r1, r2, r3], [e1, e2, e3]] {
	[[r1, e1], [r2, e2], [r3, e3]] = triple(step, seeds);
};

generate(seeds, n) = generate(seeds, [], triangles(seeds), n);
generate(seeds, rs, es, 0) = [rs, es];
generate(seeds, rs, es, n) = generate(nes, triangles(nrs) ++ rs, triangles(nes) ++ es, n - 1) {
	[nrs, nes] = steps(seeds);
};

seeds = [[t, r, l], [r, l, t], [l, t, r]];
[rs, es] = generate(seeds, 10);

fill "green" rs;
fill "red" es;
