triangle(a, b, c) = [a -- b, b -- c, c -- a];
sierpinski(_, _, con, 0) = con;
sierpinski([], nts, con, n) = sierpinski(nts, [], con, n - 1);
sierpinski([l, r, t] : ts, nts, con, n) = sierpinski(ts, lTri : rTri : tTri : nts, tri : con, n) {
	lr = l -*- r;
	lt = l -*- t;
	rt = r -*- t;
	tri = triangle(lt, rt, lr);

	lTri = [l, lr, lt];
	rTri = [lr, r, rt];
	tTri = [lt, rt, t];
};

l = c3;
r = c4;
t = l @ r & r @ l > c1;

fill "blue" triangle(l, r, t);

# Be careful about increasing this number ;p
fill "green" sierpinski([[l, r, t]], [], [], 8);
