# Origin
o = (c1 -*- c2) -*- (c3 -*- c4);

# Creates a Lauburu head given the scalp and tip points
head(scalp, tip) = [large, convex, concave] {
	center = tip -*- scalp;                        # Center
	large = center @ [scalp, tip];                 # Create large convex arc
	convex = (center -*- scalp) @ [center, scalp]; # Create small convex arc
	concave = (tip -*- center) @ [center, tip];    # Create small concave arc
};

# Create four heads with their scalps at the four edges,
# and their tips at the origin
fill "purple" [
	head(c1 -*- c2, o),
	head(c2 -*- c3, o),
	head(c3 -*- c4, o),
	head(c4 -*- c1, o)
];
