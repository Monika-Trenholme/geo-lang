# Makes 2^p points, should be greater or equal to 2
p = 10;

# Multiplication factor
f = 2;

rev(xs) = rev(xs, []);
rev([], acc) = acc;
rev(x : xs, acc) = rev(xs, x : acc);

tail(x : y) = y;

half(center, circle, p : ps, n) = half(center, circle, p, p, ps, [], n);
half(_, _, last, _, points, _, 0) = last : points;
half(center, circle, fst, pre, [], next, n) =
	half(center, circle, fst, fst, tail(rev(mid : pre : next)), [], n -1)
{
	mid = (pre -*- fst) <-> center & circle > pre;
};
half(center, circle, fst, pre, p:ps, next, n) = half(center, circle, fst, p, ps, mid : pre : next, n) {
	mid = (pre -*- p) <-> center & circle > p;
};

center = c1 -*- c3;
circle = center @ ((c1 -*- c2) -*- center);

seed = [
	(c3 -*- c4) -*- center,
	(c4 -*- c1) -*- center,
	(c1 -*- c2) -*- center,
	(c2 -*- c3) -*- center
];

points = half(center, circle, seed, p - 2);

power(n, m) = power(1, n, m);
power(acc, _, 0) = acc;
power(acc, n, m) = power(acc * n, n, m - 1);

ps = power(2, p);

lines(points, n, f) = lines(points, n, f, []);
lines(_, 0, _, acc) = acc;
lines(points, n, f, acc) = lines(points, n - 1, f, line : acc) {
	line = points !! (n % ps) -- points !! (n * f % ps);
};

stroke "blue" "0.0075%" lines(points, ps - 1, f);
